/*
 *     Copyright (c) 2017 Miroslav Mazel.
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.enjoyingfoss.om

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Build
import android.os.Bundle
import android.os.IBinder
import android.os.Parcelable
import android.support.v7.app.AppCompatActivity
import android.text.SpannableString
import android.text.Spanned
import android.view.View
import android.widget.Button
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity(), ServiceConnection, AudioContract.Presenter {
    private var savedState: Parcelable? = null

    private val SAVED_STATE = "state"

    override fun onAudioPlayed() {
        runOnUiThread {
            resetButton.visibility = View.VISIBLE
            settingsSection.visibility = View.GONE
            playButton.setImageResource(R.drawable.ic_pause_96dp)
            println("onPlay")
        }
    }

    override fun onAudioPaused() {
        runOnUiThread {
            resetButton.visibility = View.VISIBLE
            settingsSection.visibility = View.GONE
            playButton.setImageResource(R.drawable.ic_play_96dp)
            println("onPause")
        }
    }

    override fun onAudioReset() {
        runOnUiThread {
            println("onReset")
            resetButton.visibility = View.GONE
            settingsSection.visibility = View.VISIBLE
            playButton.setImageResource(R.drawable.ic_play_96dp)
        }
    }

    override fun savePausedState(state: Parcelable) {
        savedState = state
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (savedInstanceState != null)
            savedState = savedInstanceState.getParcelable(SAVED_STATE)
    }

    override fun onStart() {
        super.onStart()

        bindService(
                Intent(this, AudioService::class.java),
                this, Context.BIND_AUTO_CREATE
        )
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        outState.putParcelable(SAVED_STATE, savedState)
    }

    override fun onStop() {
        super.onStop()

        startService(Intent(this, AudioService::class.java))
        unbindService(this)
    }

    override fun onServiceConnected(name: ComponentName?, service: IBinder) {
        setOnClickListeners((service as AudioService.OmBinder))
        service.setPresenter(this, savedState)
        println("service connected")
    }

    private fun setOnClickListeners(service: AudioService.OmBinder) {
        guidedButton.setOnClickListener {
            service.setMeditationType(MeditationType.GUIDED)
        }
        unguidedButton.setOnClickListener {
            service.setMeditationType(MeditationType.UNGUIDED)
        }
        playButton.setOnClickListener {
            service.togglePlayPause()
        }
        resetButton.setOnClickListener {
            service.stop()
        }
    }

    override fun onMeditationSet(meditation: MeditationItem, type: MeditationType) {
        setUpMeditationHeading(meditation)
        when (type) {
            MeditationType.GUIDED -> toggleButtons(oldView = unguidedButton, newView = guidedButton)
            MeditationType.UNGUIDED -> toggleButtons(oldView = guidedButton, newView = unguidedButton)
        }
    }

    private fun setUpMeditationHeading(meditation: MeditationItem) {
        runOnUiThread {
            val meditationTitle = SpannableString(resources.getText(meditation.titleString))
            val dottedUnderlineSpan = CenteredBackgroundSpan(this)

            meditationTitle.setSpan(dottedUnderlineSpan, 0, meditationTitle.length, Spanned.SPAN_INCLUSIVE_INCLUSIVE)

            meditationHeadingButton.text = meditationTitle

            meditationHeadingButton.setOnClickListener {
                val detailIntent = Intent(this, MeditationDetailActivity::class.java)
                detailIntent.putExtra(MeditationDetailActivity.MEDITATION_EXTRA, meditation)
                startActivity(detailIntent)
            }
        }
    }

    private fun toggleButtons(oldView: Button, newView: Button) {
        runOnUiThread {
            if (Build.VERSION.SDK_INT < 23) {
                newView.setTextAppearance(this, R.style.RadioBorderlessOn)
                oldView.setTextAppearance(this, R.style.RadioBorderless)
            } else {
                newView.setTextAppearance(R.style.RadioBorderlessOn)
                oldView.setTextAppearance(R.style.RadioBorderless)
            }
        }
    }

    override fun onServiceDisconnected(name: ComponentName?) { //todo anything?
    }
}