/*
 *     Copyright (c) 2017 Miroslav Mazel.
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.enjoyingfoss.om

import android.os.Parcelable

/**
@author Miroslav Mazel
 */
interface AudioContract { //todo tests
    interface External {
        fun togglePlayPause()
        fun stop()
        fun setPresenter(callback: Presenter, savedState: Parcelable?)
        fun setMeditationType(meditationType: MeditationType)
    }

    interface Presenter {
        fun onAudioPlayed()
        fun onAudioPaused()
        fun onAudioReset()
        fun onMeditationSet(meditation: MeditationItem, type: MeditationType)
        fun savePausedState(state: Parcelable)
    }
}